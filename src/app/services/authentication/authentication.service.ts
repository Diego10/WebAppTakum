import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { LocalStorage } from '@ngx-pwa/local-storage';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

	path : string = "http://localhost:8001/";
	//private current_user : any;

  	constructor( private http: Http,
                 private localStorage: LocalStorage ) { }

	createAuthorizationHeader( user_data ) {
		let headers = new Headers();
		headers.append('Authorization', 'Basic ' +
		btoa( user_data.nickname + ':' + user_data.contrasena ) );
		return headers;
	}
	setLSUser( user_data ){
    	return this.localStorage.setItem('user', user_data);
	}
	getLStUser(){
	   return this.localStorage.getItem('user');
	}
	clearLStUser(){
	    return this.localStorage.removeItem('user');
	}
	/*
	setCurrentUser( user_data ){
    	this.current_user = user_data;
	}
	getCurrentUser(){
	    return this.current_user;
	}
	*/

	loginUser( user_data ){
		let options = new RequestOptions({ headers: this.createAuthorizationHeader(user_data) });
        return this.http.post( this.path + "users", null, options )
    			.do( this.logResponse )
    			.map( this.extractData )
    			.catch( this.catchError );
	}

	///*************************************************************************************************///

	logResponse( res : Response ){
		//console.log(res);
	}
	extractData( res : Response ){
		return res.json();
	}
	catchError( error : any ){
        return Observable.throw( error );
	}
}
