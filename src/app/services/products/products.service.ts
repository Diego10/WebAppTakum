import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

	path : string = "http://localhost:8001/";
	constructor( private http: Http) {
	}
	createAuthorizationHeader( user_data ) {
		let headers = new Headers();
		headers.append('Authorization', 'Basic ' +
		btoa( user_data.nickname + ':' + user_data.contrasena ) );
		return headers;
	}

	getProducts( user_data ){
		let options = new RequestOptions({ headers: this.createAuthorizationHeader(user_data) });
        return this.http.get( this.path + "products", options )
    			.do( this.logResponse )
    			.map( this.extractData )
    			.catch( this.catchError );
	}
	createProduct( user_data, product ){
		let options = new RequestOptions({ headers: this.createAuthorizationHeader(user_data) });
        return this.http.post( this.path + "products", product, options )
    			.do( this.logResponse )
    			.map( this.extractData )
    			.catch( this.catchError );
	}
	getCatgs( user_data ){
		let options = new RequestOptions({ headers: this.createAuthorizationHeader(user_data) });
        return this.http.get( this.path + "categorias", options )
    			.do( this.logResponse )
    			.map( this.extractData )
    			.catch( this.catchError );
	}
	getProduct( user_data, id ){
		let options = new RequestOptions({ headers: this.createAuthorizationHeader(user_data) });
        return this.http.get( this.path + "products/" + id, options )
    			.do( this.logResponse )
    			.map( this.extractData )
    			.catch( this.catchError );
	}
	deleteProduct( user_data, id ){
		let options = new RequestOptions({ headers: this.createAuthorizationHeader(user_data) });
        return this.http.delete( this.path + "products/" + id, options )
    			.do( this.logResponse )
    			.map( this.extractData )
    			.catch( this.catchError );
	}
	///*************************************************************************************************///

	logResponse( res : Response ){
		//console.log(res);
	}
	extractData( res : Response ){
		return res.json();
	}
	catchError( error : any ){
        return Observable.throw( error );
	}

}
