import { Component } from '@angular/core';
import { LoginComponent } from './pages/login/login.component';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication/authentication.service';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
	logged = false;
  modalMessage : String = "";
  user: Object = {
    nickname: "diego10",
    contrasena: "america",
    nombre: ""
  };

 	constructor(  private router: Router, 
                private authService: AuthenticationService) {

    this.authService.getLStUser().subscribe((user) => {
       if (user) {
        this.logged = true;
        this.user = user;
       }
       else{
        this.router.navigateByUrl('login');
       }
    });
    
  }
  onLogin(user) {

    this.authService.loginUser(user).subscribe(
        auth_user => {
          this.authService.setLSUser(auth_user).subscribe(() => {
            //this.authService.setCurrentUser(auth_user);
            this.user = auth_user;
            $("#LoginModal").modal("hide");
              setTimeout(() => {
              this.logged = true;
              this.router.navigateByUrl('');
            }, 300);
          });
        },
        err => {
          this.modalMessage = err._body;
          $("#modalResult").modal("show");
        } 
    );

  }
  onLogout(){
      $("#modalLogout").modal("hide");
      this.authService.clearLStUser().subscribe(() => {
        this.logged = false;
        this.user = { nickname: null, contrasena: null, nombre: null};
        this.router.navigateByUrl('login');
      });
  }
}
