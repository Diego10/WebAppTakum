import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductsService } from '../../services/products/products.service';
import { AuthenticationService } from '../../services/authentication/authentication.service';
declare var $: any;

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {


	newProduct : Object = {
		nombre: "",
		descripcion: "",
		costo: null,
		rol_id: null,
		catg_id: null
	}

	categoriasNP : Array<Object> = [
		{ id: null, nombre: null }
	]

	objectKeys = Object.keys;
	productList: Object = {};
	user: Object;
	modalMessage: String ="";

	constructor(private router: Router, private authService: AuthenticationService, private prodsService: ProductsService) { }

	ngOnInit() {
    	this.loadProducts();
	}

	onProductDetail(product){
		console.log(product);
		this.router.navigateByUrl('product/' + product.id);
	}
	onNuevoProducto(newProd){
    	
        this.prodsService.createProduct(this.user, newProd).subscribe(
		    prod => {
		    	$("#modalNuevoProducto").modal("hide");
		    	this.modalMessage = prod.message;
		    	$("#modalResultList").modal("show");
		    	this.loadProducts();

		    	this.newProduct = { nombre: "", descripcion: "", costo: null, rol_id: null, catg_id: null };
		    },
		    err => {
		    	this.modalMessage = err._body;
		    	$("#modalResultList").modal("show");
		    } 
		);
	}
	loadProducts(){
	    this.authService.getLStUser().subscribe((user) => {
       		if (user) {
		        this.user = user;
		        this.prodsService.getProducts(user).subscribe(
				    products => {
				    	this.productList = products;
				        this.prodsService.getCatgs(this.user).subscribe(
						    catgs => {
						    	this.categoriasNP = catgs;
						    },	
						    err => {
						    	$("#modalConexError").modal("show");
						    } 
						);
				    },
				    err => {
				    	$("#modalConexError").modal("show");
				    } 
				);
       		}
	    });
	}
}
