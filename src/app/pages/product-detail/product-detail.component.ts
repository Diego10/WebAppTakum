import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../../services/products/products.service';
import { AuthenticationService } from '../../services/authentication/authentication.service';
declare var $: any;

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})

export class ProductDetailComponent implements OnInit {

	modalMessage: String = "";
	user: Object;
	product: Object = { id: null, nombre: null, descripcion: null, costo: null, rol_id: null, catg_id: null };
	resultDeleting: boolean = false;

	constructor(private router: Router, private act_router: ActivatedRoute, private authService: AuthenticationService, private prodsService: ProductsService) { }

	ngOnInit() {
		this.loadProduct();
	}
 	onDeleteProduct(){
		this.prodsService.deleteProduct(this.user, this.act_router.snapshot.paramMap.get('id')).subscribe(
		    msg => {
		    	this.resultDeleting = true;
		    	this.modalMessage = msg.message;
    			$("#modalDelete").modal("hide");
    			$("#modalResultDelete").modal("show");
    			//this.router.navigateByUrl('');
		    },
		    err => {
		    	this.resultDeleting = false;
		    	this.modalMessage = err._body;
    			$("#modalResultDelete").modal("show");
		    } 
		);
 	}
	loadProduct(){
	    this.authService.getLStUser().subscribe((user) => {
       		if (user) {
		        this.user = user;
		        this.prodsService.getProduct(this.user, this.act_router.snapshot.paramMap.get('id')).subscribe(
				    product => {
				    	this.product = product;
				    },
				    err => {
				    	$("#modalConexError").modal("show");
				    } 
				);
       		}
	    });
	}
	acceptModalSuccess(){
        $("#modalResultDelete").modal("hide");
		if (this.resultDeleting) {
              setTimeout(() => {
              this.router.navigateByUrl('');
            }, 300);
		}
	}
}
